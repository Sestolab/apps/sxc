# sxc

Simple X clipboard utility.

## Dependencies

* libxcb

## Installation

* $ make
* # make install

## Options

* `-o` - write the selection to the standard output
* `-p` - operate on the PRIMARY selection (default)
* `-c` - operate on the CLIPBOARD selection
* `-1` - exit after selection has been pasted once

## Examples

Put the file content in the PRIMARY selection:

```
$ cat file | sxc
```

Put the file content in the CLIPBOARD selection:

```
$ cat file | sxc -c
```

Put the command output in the PRIMARY selection, and run sxc in a new session:

```
$ pwd | setsid sxc &
```

Put the image in the CLIPBOARD selection:

```
$ cat file.png | sxc -ct image/png
```

Write the PRIMARY selection to standard output:

```
$ sxc -o
```

Write the PRIMARY selection to a file:

```
$ sxc -o > file
```

Write the CLIPBOARD selection to standard output:

```
$ sxc -co
```

Write the PRIMARY selection to CLIPBOARD selection:

```
$ sxc -o | sxc -c
```

