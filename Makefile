CFLAGS += -Wall -Wextra -pedantic -lxcb

PREFIX ?= /usr/local
CC ?= cc

all: sxc

sxc: sxc.c
	$(CC) sxc.c $(CFLAGS) -o sxc

install: sxc
	install -Dm755 sxc -t $(DESTDIR)$(PREFIX)/bin

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/sxc

clean:
	rm -f sxc

.PHONY: all install uninstall clean

