#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <xcb/xcb.h>


void copy(xcb_connection_t *c, xcb_window_t w, xcb_atom_t sel, xcb_atom_t target, int once)
{
	xcb_atom_t targets = xcb_intern_atom_reply(c, xcb_intern_atom(c, 0, 7, "TARGETS"), NULL)->atom;
	uint32_t max_request_len = xcb_get_maximum_request_length(c);
	char *data = NULL;
	short int should_exit = 0;
	size_t read = 0, size = BUFSIZ, length = 0;

	if ((data = malloc(size)) == NULL)
		return;

	while ((read = fread(data + length, 1, size - length, stdin)))
	{
		length += read;

		if (length + 1 < size)
			continue;

		size *= 2;

		if (length > max_request_len || (data = (char *)realloc(data, size)) == NULL)
		{
			free(data);
			return;
		}
	}

	xcb_set_selection_owner(c, w, sel, XCB_CURRENT_TIME);
	xcb_flush(c);

	if (xcb_get_selection_owner_reply(c, xcb_get_selection_owner(c, sel), NULL)->owner != w)
		should_exit = 1;

	while (!should_exit)
	{
		xcb_generic_event_t *e = xcb_wait_for_event(c);

		if (e == NULL) break;

		switch ((e->response_type & ~0x80))
		{
			case XCB_SELECTION_REQUEST:
			{
				xcb_selection_request_event_t *ne = (xcb_selection_request_event_t*) e;

				if (ne->property == XCB_NONE)
					break;

				if (ne->target == target || ne->target == XCB_ATOM_STRING)
				{
					xcb_change_property(c, XCB_PROP_MODE_REPLACE, ne->requestor, ne->property,
						ne->target, 8, length, data);
					should_exit = once;
				}
				else if (ne->target == targets)
					xcb_change_property(c, XCB_PROP_MODE_REPLACE, ne->requestor, ne->property,
						XCB_ATOM_ATOM, 32, 1, &target);

				xcb_selection_notify_event_t sn = {
					.response_type = XCB_SELECTION_NOTIFY,
					.pad0 = ne->pad0,
					.sequence = ne->sequence,
					.time = ne->time,
					.requestor = ne->requestor,
					.selection = ne->selection,
					.target = ne->target,
					.property = ne->property,
				};

				xcb_send_event(c, 0, ne->requestor, XCB_EVENT_MASK_NO_EVENT, (char *)&sn);
				xcb_flush(c);
				break;
			}
			case XCB_SELECTION_CLEAR:
				should_exit = 1;
				break;
		}
		free(e);
	}

	free(data);
}

void paste(xcb_connection_t *c, xcb_window_t w, xcb_atom_t sel)
{
	xcb_convert_selection(c, w, sel, XCB_ATOM_STRING, sel, XCB_CURRENT_TIME);
	xcb_flush(c);

	xcb_generic_event_t *e = NULL;
	do { e = xcb_wait_for_event(c); }
	while (e && (e->response_type & ~0x80) != XCB_SELECTION_NOTIFY);

	if (!e || ((xcb_selection_notify_event_t *)e)->property == XCB_NONE)
	{
		free(e);
		return;
	}
	free(e);

	xcb_get_property_reply_t *r = NULL;
	size_t read = 0;
	size_t read_to = 0;

	while ((r = xcb_get_property_reply(c, xcb_get_property(c, 0, w, sel, XCB_ATOM_ANY, read, read_to), NULL)))
	{
		fwrite(xcb_get_property_value(r), 1, r->value_len, stdout);
		read += r->value_len;
		read_to = r->bytes_after;
		free(r);
	}
	free(r);
}

int main(int argc, char *argv[])
{
	xcb_connection_t *c = xcb_connect(NULL, NULL);

	if (xcb_connection_has_error(c))
	{
		fprintf(stderr, "Could not connect to the X server.\n");
		xcb_disconnect(c);
		return 1;
	}

	xcb_atom_t target = xcb_intern_atom_reply(c, xcb_intern_atom(c, 0, 11, "UTF8_STRING"), NULL)->atom;
	xcb_atom_t sel = xcb_intern_atom_reply(c, xcb_intern_atom(c, 0, 7, "PRIMARY"), NULL)->atom;
	xcb_screen_t *s = xcb_setup_roots_iterator(xcb_get_setup(c)).data;
	xcb_window_t w = xcb_generate_id(c);
	xcb_create_window(c, 0, w, s->root, 0, 0, 1, 1, 0, XCB_WINDOW_CLASS_INPUT_OUTPUT, s->root_visual, 0, NULL);

	int opt, output = 0, once = 0;

	while ((opt = getopt(argc, argv, "cot:1")) != -1)
	{
		switch (opt)
		{
			case 'c':
				sel = xcb_intern_atom_reply(c, xcb_intern_atom(c, 0, 9, "CLIPBOARD"), NULL)->atom;
				break;
			case 'o':
				output = 1;
				break;
			case 't':
				target = xcb_intern_atom_reply(c, xcb_intern_atom(c, 0, strlen(optarg), optarg), NULL)->atom;
				break;
			case '1':
				once = 1;
				break;
		}
	}

	output ? paste(c, w, sel) : copy(c, w, sel, target, once);

	xcb_destroy_window(c, w);
	xcb_disconnect(c);
	return 0;
}

